﻿using System;

namespace M17_UF1_E1_MyVector_ReinaCarlos
{
    public class Menu
    {
        public static void Inici()
        {
            bool fi;
            do
            {
                MostrarMenu();
                fi = TractarOpcio();
            } while (!fi);
        }

        public static void MostrarMenu()
        {
            Console.WriteLine("======================================================================");
            Console.WriteLine("EXERCICIS INICIALS");
            Console.WriteLine("Escull un programa: ");
            Console.WriteLine("1. Hello {name}                     2. MyVector");
            Console.WriteLine("3. VectorGame");
            Console.WriteLine("Escriu 'Exit' per sortir");
            Console.WriteLine("======================================================================");
        }

        public static bool TractarOpcio()
        {
            var fi = false;
            var opcio = Console.ReadLine();
            switch (opcio.ToUpper())
            {
                case "1":
                    Console.Write("Escriu el teu nom: ");
                    var nom = Console.ReadLine();
                    Console.WriteLine("Hello " + nom);
                    break;
                case "2":
                    Console.WriteLine("Escriu les coordenades del vector:");
                    Console.WriteLine("Punt inici (x1, y1)");
                    var x1 = Convert.ToDouble(Console.ReadLine());
                    var y1 = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Punt final (x2, y2)");
                    var x2 = Convert.ToDouble(Console.ReadLine());
                    var y2 = Convert.ToDouble(Console.ReadLine());
                    var myVector = new MyVector(x1, y1, x2, y2);
                    Console.WriteLine("Vector: \n" + myVector);
                    Console.WriteLine("Distancia del vector: " + myVector.VectorLength());
                    myVector.ChageVectorDirection();
                    Console.WriteLine("Vector després de canviar direcció: \n" + myVector);
                    break;
                case "3":
                    Console.WriteLine("Escriu la longitud del array de vectors:");
                    var n = Convert.ToInt32(Console.ReadLine());
                    MyVector[] arr = VectorGame.RandomVectors(n);
                    var a = true;
                    while (a)
                    {
                        Console.WriteLine("Escull una opció: \n 1- Filtrar per distancia dels vectors \n 2- Filtrar per proximitat a l'origen \n 3- Sortir");
                        var sortOption = Convert.ToInt32(Console.ReadLine());
                        switch (sortOption)
                        {
                            case 1:
                                VectorGame.SortVectors(arr, true);
                                break;
                            case 2:
                                VectorGame.SortVectors(arr, false);
                                break;
                            case 3:
                                a = false;
                                break;
                            default:
                                Console.WriteLine("Escull una opció vàlida");
                                break;
                        }
                    }
                    break;
                case "EXIT":
                    fi = true;
                    break;
                default:
                    Console.WriteLine("Escull una opció vàlida");
                    break;
            }

            return fi;
        }
    }
}
