﻿using System;

namespace M17_UF1_E1_MyVector_ReinaCarlos
{
    public class MyVector
    {
        public double x1;
        public double y1;
        public double x2;
        public double y2;

        public MyVector() { }

        public MyVector(double X1, double Y1, double X2, double Y2)
        {
            x1 = X1;
            y1 = Y1;
            x2 = X2;
            y2 = Y2;
        }

        public void SetX1(double X1)
        {
            x1 = X1;
        }

        public double GetX1()
        {
            return x1;
        }

        public void SetY1(double Y1)
        {
            y1 = Y1;
        }

        public double GetY1()
        {
            return y1;
        }

        public void SetX2(double X2)
        {
            x2 = X2;
        }

        public double GetX2()
        {
            return x2;
        }

        public void SetY2(double Y2)
        {
            y2 = Y2;
        }

        public double GetY2()
        {
            return y2;
        }
        
        public double VectorLength()
        {
            return Math.Sqrt(Math.Pow((x2 - x1), 2) + Math.Pow((y2 - y1), 2));
        }

        public void ChageVectorDirection()
        {
            x1 *= -1;
            y1 *= -1;
            x2 *= -1;
            y2 *= -1;
        }

        public override string ToString()
        {
            return "Punt inici: (" + x1 + ", " + y1 + ")\n"
                 + "Punt final: (" + x2 + ", " + y2 + ")\n";
        }

    }
}
