﻿using System;

namespace M17_UF1_E1_MyVector_ReinaCarlos
{
    public class VectorGame
    {
        public VectorGame()
        {

        }

        public static MyVector[] RandomVectors(int n)
        {
            Console.WriteLine(n + " vectors aleatoris generats:");
            Random random = new Random();
            MyVector[] arrayVector = new MyVector[n];
            MyVector vector;

            for (int i = 0; i < n; i++)
            {
                vector = new MyVector(random.Next(-99, 99), random.Next(-99, 99), random.Next(-99, 99), random.Next(-99, 99));
                arrayVector[i] = vector;
                vector.VectorLength();
                vector.VectorLength();
            }
            
            foreach (MyVector i in arrayVector)
            {
                Console.WriteLine(i);
            }

            return arrayVector;

        }

        public static MyVector[] SortVectors(MyVector[] arr, bool x)
        {
            MyVector[] res = arr;
            if (x == true)
            {
                MyVector tmp;
                for (int i = 0; i < res.Length - 1; i++)
                {
                    for (int j = 0; j < res.Length - 1; j++)
                    {
                        if (arr[j].VectorLength() < arr[j + 1].VectorLength())
                        {
                            tmp = res[j + 1];
                            res[j + 1] = res[j];
                            res[j] = tmp;
                        }
                    }
                }
                
                Console.WriteLine("\nOrdenació per distanica dels vectors: ");
                foreach (MyVector i in res)
                {
                    Console.WriteLine(i);
                }
            }
            else
            {
                double[] origin = new double[arr.Length];
                MyVector temp;
                double a;
                double b;
                for (int i = 0; i < origin.Length; i++)
                {
                    a = Math.Sqrt(Math.Pow((arr[i].x1), 2) + Math.Pow((arr[i].y1), 2));
                    b = Math.Sqrt(Math.Pow((arr[i].x2), 2) + Math.Pow((arr[i].y2), 2));
                    if (a < b)
                    {
                        origin[i] = a;
                    }
                    else
                    {
                        origin[i] = b;
                    }
                }
                for (int i = 0; i < res.Length + 1; i++)
                {
                    for (int j = i + 1; j < res.Length; j++)
                    {
                        if (origin[i] > origin[j])
                        {
                            temp = res[i];
                            res[i] = res[j];
                            res[j] = temp;
                        }
                    }
                }
                
                Console.WriteLine("\nOrdenació per proximitat a l'origen: ");
                foreach (MyVector i in res)
                {
                    Console.WriteLine(i);
                }
            }
            return res;
        }
    }
}
